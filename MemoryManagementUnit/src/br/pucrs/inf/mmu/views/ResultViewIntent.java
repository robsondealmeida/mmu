package br.pucrs.inf.mmu.views;

public class ResultViewIntent extends Intent {

	@Override
	public void execute() {
		
		this.displayInstruction();
	}

	private void displayInstruction() {
		
		this.console.writeLine("");
		this.console.writeLine("Instruções para visualização dos resultados:");
		this.console.writeLine("Para avançar um passo na execução, utilize a tecla 'n' ([n]ext).");
		this.console.writeLine("Para encerrar, ainda vale digitar 'q'.");
		this.console.writeLine("");
		this.console.writeLine("Iniciando visualização ...");
		this.console.writeLine("");
	}
}
