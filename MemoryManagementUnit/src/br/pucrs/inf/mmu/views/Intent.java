package br.pucrs.inf.mmu.views;



/**
 * Define as operações e propriedades das interações com o usuário.
 * 
 * @author robsondealmeida
 */
public abstract class Intent {

	
	protected Console console;
	
	
	public Intent() {
		this.console = new Console();
	}
	
	
	
	/**
	 * Executa a interação com o usuário.
	 */
	public abstract void execute();
}
