package br.pucrs.inf.mmu.views;



/**
 * Responsável pelas interações da aplicação com o usuário.
 * 
 * @author robsondealmeida
 */
public class ApplicationIntent extends Intent {

	@Override
	public void execute() {
		this.displayApplicationInfo();
		this.displayGeneralInstructions();
	}
	
	
	
	
	private void displayGeneralInstructions() {
		this.console.writeLine("");
		this.console.writeLine("Instruções:");
		this.console.writeTabuled("Siga o fluxo de instruções do programa.");
		this.console.writeTabuled("Para sair basta teclar 'q'.");
	}


	private void displayApplicationInfo() {
		this.console.writeLine("");
		this.console.writeLine("");
		this.console.writeLine("Gerenciamento de memórias");
		this.console.writeLine("Sistemas Operacionais, Ciência da Computação");
		this.console.writeLine("Faculdade de Informática - PUCRS");
		this.console.writeLine("Alunos: Marcos Trajano, Robson de Almeida. Prof: Edson Moreno");
		this.console.writeLine("versão: 1.0");
		this.console.writeLine("----------------------------------------------------------");
	}

}
