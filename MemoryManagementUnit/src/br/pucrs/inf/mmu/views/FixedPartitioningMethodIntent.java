package br.pucrs.inf.mmu.views;

public class FixedPartitioningMethodIntent extends Intent {

	@Override
	public void execute() {
		
		// armazena a entrada do usuário ...
		String lenght = this.console.readLine("Digite o tamanho da partição");
		
		
		// ... e salva na sessão para posteriormente ser referenciada ...
		Session.getCurrent().put(
				FixedPartitioningMethodIntent.class.getName(), Integer.valueOf(lenght));
	}
}
