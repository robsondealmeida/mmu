package br.pucrs.inf.mmu.views;



/**
 * Trata as interações com usuário para as requisições de alocação de memória.
 * 
 * @author robsondealmeida
 *
 */
public class AllocationRequestIntent extends Intent {

	@Override
	public void execute() {
		
		// exibe as instruções para o usuário ...
		this.displayInstructions();
		
		
		// armazena a entrada do usuário ...
		String file = this.console.readLine("Digite o nome do arquivo");
		
		
		// guarda na sessão o nome do arquivo para posterior referência ...
		Session.getCurrent().put(AllocationRequestIntent.class.getName(), file);
	}

	private void displayInstructions() {
		
		this.console.writeLine("");
		this.console.writeLine("A sequência de requisições de alocação e liberação de memória, " + 
				"devem estar em um arquivo localizado no mesmo diretório da aplicação.");
	}

}
