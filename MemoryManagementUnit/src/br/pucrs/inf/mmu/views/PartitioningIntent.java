package br.pucrs.inf.mmu.views;

public class PartitioningIntent extends Intent {

	@Override
	public void execute() {
		
		this.displayPartitioningInstructions();
		
		
		
		// lê o tipo de particionamento ...
		String type = this.console.readLine("Tipo");
		
		
		// guarda as informações na sessão ...
		Session.getCurrent().put(PartitioningIntent.class.getName(), type.charAt(0));
	}
	
	
	
	private void displayPartitioningInstructions() {
		this.console.writeLine("");
		this.console.writeLine("Selecione o tipo de particionamento da memória:");
		this.console.writeTabuled("'f' - para particionamento fixo,");
		this.console.writeTabuled("'v' - para particionamento variado, e");
		this.console.writeTabuled("'b' - para buddy.");
	}
}
