package br.pucrs.inf.mmu.views;

import java.util.HashMap;
import java.util.Map;

public class Session {

	private Map<String, Object> items;
	private static Session instance;
	
	
	private Session() {
		this.items = new HashMap<String, Object>();
	}
	
	
	public static Session getCurrent() {
		
		if (instance == null) {
			instance = new Session();
		}
		
		
		return instance;
	}
	
	
	public void put(String key, Object value) {
		this.items.put(key, value);
	}
	
	public Object get(String key) {
		return this.items.get(key);
	}


	public void remove(String key) {
		this.items.remove(key);
	}
}
