package br.pucrs.inf.mmu.views;

public class MemoryIntent extends Intent {

	@Override
	public void execute() {
		
		// lê o tamanho da memória ...
		String memory = this.console.readLine("Tamanho da memória");
		
		
		
		// aramazena na sessão ..
		Session.getCurrent().put(MemoryIntent.class.getName(), memory);
	}
}
