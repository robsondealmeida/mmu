package br.pucrs.inf.mmu.views;

import java.io.IOException;

import br.pucrs.inf.mmu.controllers.OperationalSystemController;
import br.pucrs.inf.mmu.models.FixedPartitionMethod;
import br.pucrs.inf.mmu.models.PartitioningMethod;
import br.pucrs.inf.mmu.models.VariablePartitionMethod;


public class Application {
	
	
	private Console console;
	private OperationalSystemController controller;
	
	

	public void run() {
		
		// inicializa o console responsável pela entrada e saída de dados do usuário ...
		this.console = new Console();
		
		// instancia o controle que encapsula as chamadas da aplicação ao modelo ...
		this.controller = new OperationalSystemController();
		
		
		
		
		// escreve o cabeçalho e as instruções gerais da aplicação ...
		this.call(new ApplicationIntent());
		
		
		// configura o tamanho total da memória ...
		this.runMemorySizeDefinition();
		
		
		// roda os procedimentos necessários para definição 
		// do tipo de método de particionamento da memória ...
		this.runPartitionMethodDefinition();
		
		
			

		// lê o arquivo com as definições de requisições 
		// de alocação e liberação ...
		this.runAllocationRequests();
		

		
		// step by step ...
		this.runResultView();
		
		
		
		
		// termina a aplicação ...
		this.quit();
	}

	
	
	private void runMemorySizeDefinition() {
		
		this.call(new MemoryIntent());
		
		try {
			
			int memorySize = Integer.valueOf(Session.getCurrent().get(MemoryIntent.class.getName()).toString());
		
			this.controller.setMemorySize(memorySize);
			
			
		} catch (NumberFormatException e) {
			
			char q = Session.getCurrent().get(MemoryIntent.class.getName()).toString().charAt(0);
			
			if (q == 'q')
				this.quit();
		
		} catch (Exception e) {
			this.console.writeError(e.getMessage());
			this.quit();
		}
	}

	private void runResultView() {
		
		// exibe as instruções ...
		this.call(new ResultViewIntent());
		
		
		
		// exibe o status da memória antes das requisições ...
		try {
			String memoryStatus = this.controller.buildMemoryRepresentation();
			this.console.writeLine(memoryStatus);
			
		} catch (Exception e) {
			this.console.writeError(e.getMessage());
			this.quit();
		}
		
		
		
		// inicia execução do passo-a-passo ...
		char input = 0;
		
		do {
			
			input = this.console.readLine().charAt(0);
			
			
			switch (input) {
				case 'q': this.quit(); break;
				case 'n': this.step(); break;
				default: 
					this.console.writeError(
							"Comando não reconhecido. São reconhecidos apenas os comandos de " + 
							"[q]uit e [n]ext. Favor, digite uma das letras entre colchetes."
						);
					break;
			}
			
		} while (input != 'q');
		
		
		
		this.quit();
	}

	private void step() {
		
		String step = "";
		
		try {
			
			step = this.controller.step();
			
			this.console.writeLine(step);
			
			
		} catch (Exception e) {
			this.console.writeError(e.getMessage());
			this.quit();
		}
	}

	private void runAllocationRequests() {
		
		// invoca a intent para o usuário informar o arquivo ...
		this.call(new AllocationRequestIntent());
		
		
		// recupera o nome do arquivo digitado pelo usuário ...
		String file = (String)Session.getCurrent().get(AllocationRequestIntent.class.getName());
		

		try {
			
			this.console.writeLine("Lendo arquivo ...");
			
			// carrega as requisições a partir do arquivo ...
			this.controller.loadRequests(file);
			
			
			this.console.writeLine("Pronto. Requisições carregadas.");
			
		} catch (IOException e) {
			this.console.writeError(e.getMessage());
			this.quit();
		} catch (Exception e) {
			this.console.writeError(e.getMessage());
			this.quit();
		}
	}

	private void runPartitionMethodDefinition() {
		
		try {
			// chama a intent que lida com o particionamento ...
			this.call(new PartitioningIntent());
			
			
			// recupera o tipo de particionamento selecionado pelo usuário ...
			char partitioningType = 
					(Character) Session.getCurrent().get(PartitioningIntent.class.getName());
			
			
			
			if (partitioningType == 'q')
				this.quit();
			
			
			
			this.controller.setPartitioningMethod(partitioningType);
			
			
			
			// remove o tipo de particionamento da sessão ...
			Session.getCurrent().remove(PartitioningIntent.class.getName());
			
			
			
			
			PartitioningMethod partitioningMethod = this.controller.getPartitioningMethod();
			
			
			switch (partitioningMethod.getPartitioningMethodType()) {
				
				case FIXED:
					// chama a intent para o método de particionamento fixo ...
					this.call(new FixedPartitioningMethodIntent());
					
					// recupera o tamanho da partição digitada pelo usuário ...
					int partitioningSize =
							(Integer) Session.getCurrent().get(FixedPartitioningMethodIntent.class.getName());
					
					
					
					// define o tamanho da particição no método ...
					((FixedPartitionMethod)partitioningMethod).setPartitioningSize(partitioningSize);
					
					
					
					
					// limpa a sessão ...
					Session.getCurrent().remove(FixedPartitioningMethodIntent.class.getName());
					
					
					break;
					
				case VARIABLE:
					
					// chama a intent para as instruções de método de particionamento variável ...
					this.call(new VariablePartitionMethodIntent());
					
					// recupera a política de alocação selecionado pelo usuário ...
					char policy = 
							(Character)Session.getCurrent().get(VariablePartitionMethodIntent.class.getName());
					
					
					// define a política de alocação do método ....
					((VariablePartitionMethod)partitioningMethod).setPolicy(policy);
					
					
					// limpa a sessão ...
					Session.getCurrent().remove(VariablePartitionMethodIntent.class.getName());
					
					break;
					
					
					
					
				default:
					break;
			}
			
			
			// incializa a memória ...
			this.controller.formatMemory();
			
			
			
			
		} catch (IllegalArgumentException e) {
			this.console.writeError(e.getMessage());
			this.quit();
		} catch (Exception e) {
			this.console.writeError(e.getMessage());
			this.quit();
		}
	}



	/**
	 * Invoca uma intent.
	 * */
	private void call(Intent intent) {
		intent.execute();
	}
	

	/**
	 * Encerra a aplicação.
	 */
	private void quit() {
		this.console.writeLine("");
		this.console.writeLine("");
		this.console.writeLine("Fim da execução.");
		System.exit(0);
	}


	


	
}
