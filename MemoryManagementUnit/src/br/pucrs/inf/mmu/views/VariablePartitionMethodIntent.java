package br.pucrs.inf.mmu.views;

public class VariablePartitionMethodIntent extends Intent {

	@Override
	public void execute() {
		
		// exibe as instruções para seleção da política de alocação ...
		this.displayVariablePartitioningMethodPolicyInstructions();
		
		
		// pega a entrada do usuário ...
		String policy = this.console.readLine("Política de alocação");
		
		// salva a entrada do usuário para posterior referência ...
		Session.getCurrent().put(VariablePartitionMethodIntent.class.getName(), policy.charAt(0));
	}

	private void displayVariablePartitioningMethodPolicyInstructions() {
		
		this.console.writeLine("");
		this.console.writeLine(
				"Para selecionar uma política de alocação, " + 
				"digite a primeira letra do nome da política desejada: " + 
				"[b]est-fit, [w]orst-fit, [f]irst-fit ou [n]ext-fit."
			);
	}
}
