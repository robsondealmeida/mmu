package br.pucrs.inf.mmu.views;

import java.util.Scanner;

public class Console {

	
	private Scanner scanner;
	
	
	public Console() {
		this.scanner = new Scanner(System.in);
	}
	
	
	public String readLine() {
		return this.scanner.nextLine();
	}
	
	public String readLine(String label) {
		this.writeLabel(label);
		return this.scanner.nextLine();
	}
	
	public void writeLine(String value) {
		System.out.println(value);
	}
	
	public void writeLabel(String label) {
		System.out.print(label + ": ");
	}
	
	public void writeTabuled(String value) {
		System.out.println("\t" + value);
	}


	public void writeError(String message) {
		System.out.println("");
		System.err.println(message);
		System.out.println("");
	}
}
