package br.pucrs.inf.mmu.controllers;



public class MemoryPresenterFactory {

	
	private static MemoryPresenterFactory memoryPresenterFactory;

	
	private MemoryPresenterFactory() {  }
	
	
	
	public static MemoryPresenterFactory getInstance() {
	
		if (memoryPresenterFactory == null)
			memoryPresenterFactory = new MemoryPresenterFactory();
		
		
		return memoryPresenterFactory;
	}



	public MemoryPresenter create(char type) {
		
		switch (type) {
			case 'f': return new FixedPartitioningPresenter();
			case 'v': return new VariablePartitioningPresenter();
			case 'b': return new BuddyPartitioningPresenter();
			default:
				throw new IllegalArgumentException(
						"Tipo de particionamento não reconhecido."
				);
		}
	}
}
