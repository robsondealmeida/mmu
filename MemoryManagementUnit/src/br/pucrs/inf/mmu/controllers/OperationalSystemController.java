package br.pucrs.inf.mmu.controllers;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;

import br.pucrs.inf.mmu.models.AllocationMemoryException;
import br.pucrs.inf.mmu.models.AllocationRequest;
import br.pucrs.inf.mmu.models.EndOfExecutionException;
import br.pucrs.inf.mmu.models.NoPartitionFound;
import br.pucrs.inf.mmu.models.OperationalSystem;
import br.pucrs.inf.mmu.models.PartitioningMethod;
import br.pucrs.inf.mmu.models.PartitioningMethodFactory;
import br.pucrs.inf.mmu.models.Proccess;
import br.pucrs.inf.mmu.models.ReleaseMemoryException;
import br.pucrs.inf.mmu.models.ReleaseRequest;
import br.pucrs.inf.mmu.models.Request;
import br.pucrs.inf.mmu.models.RequestType;
import br.pucrs.inf.mmu.models.RequestTypeNotDefined;



/**
 * Controlador para o sistema operacional.
 * 
 * @author robsondealmeida
 */
public class OperationalSystemController {

	
	private OperationalSystem os;
	private MemoryPresenter presenter;
	private Queue<Request> startRequests;
	
	
	
	
	
	public OperationalSystemController() {
		this.os = new OperationalSystem();
	}
	
	
	

	
	
	/**
	 * Define o método de particionamento de memória utilizado pelo sistema operacional.
	 * 
	 * @param type é a representação em caracter do tipo de método.
	 */
	public void setPartitioningMethod(char type) throws IllegalArgumentException {
		this.os.setPartitioningMethod(PartitioningMethodFactory.getInstance().create(type));
		this.presenter = MemoryPresenterFactory.getInstance().create(type);
	}



	/**
	 * Retorna o tipo de método de particionamento utilizado pelo sistema operacional.
	 * */
	public PartitioningMethod getPartitioningMethod() {
		return this.os.getPartitioningMethod();
	}



	/**
	 * Carrega as requisições de alocação e liberação de memória
	 * a partir do arquivo passado por argumento.
	 * 
	 * @param file nome do arquivo que contém as requisições.
	 * @throws IOException se o arquivo não for encontrado.
	 * */
	public void loadRequests(String file) throws IOException {
		
		
		// buffer para leitura do arquivo ...
		BufferedReader reader = new BufferedReader(new FileReader(file));
		
		
		String line = "";
		
		// enquanto houver linha não lida ...
		while ((line = reader.readLine()) != null) {
			
			Request request = null;
			Proccess proccess = new Proccess();
			String proccessId = "";
			
			// se a linha começar com "IN", é uma requisição de alocação ...
			if (line.startsWith("IN")) {
				
				request = new AllocationRequest();
				request.setType(RequestType.IN);
				
				
				// lê do arquivo o identificador do processo ...
				proccessId = line.substring(3, 4);
				
				// lê do arquivo o tamanho ocupado pelo processo ...
				String proccessSize = line.substring(5, (line.length() - 1));

				proccess.setSize(Integer.valueOf(proccessSize));
				
			
			// se a linha começar com "OUT", é uma requisição de liberação ...
			} else if (line.startsWith("OUT")) {
				
				request = new ReleaseRequest();
				request.setType(RequestType.OUT);
				
				
				// lê do arquivo o identificador do processo ...
				proccessId = line.substring(4, 5);
			}
			
			
			proccess.setId(proccessId);
			request.setRepresentation(line);
			
			
			// vincula o processo à requisição ..
			request.attach(proccess);
			
			// adiciona a requisição na fila do sistema ...
			this.os.addRequest(request);
		}
		
		
		reader.close();
		
		
		// copio as requisições para possibilitar o 
		// reinício da excução do passo-a-passo ...
		this.startRequests = new LinkedList<Request>();
		for (Request request : this.os.getRequests())
			this.startRequests.add(request);
	}






	public void setMemorySize(int memorySize) {
		this.os.setMemorySize(memorySize);
	}






	public String step() throws NoPartitionFound, AllocationMemoryException, ReleaseMemoryException, 
							RequestTypeNotDefined, EndOfExecutionException {
		
		
		if (this.os.getRequests().isEmpty())
			throw new EndOfExecutionException("Fim das requisições.");
		
		
		
		Request request = this.os.getRequests().poll();
		
		this.os.proccessRequest(request);
		
		
		return request.getRepresentation() + "\t" + this.buildMemoryRepresentation();
	}






	public String buildMemoryRepresentation() throws NoPartitionFound {
		
		
		return this.presenter.render(this.os.getMemory(), this.os.getPartitioningMethod());
		
	}



	/**
	 * Formata a memória de acordo com o tipo de particionamento.
	 * */
	public void formatMemory() {
		
		switch (this.os.getPartitioningMethod().getPartitioningMethodType()) {
			case FIXED: this.os.makeFixed(); break;
			case VARIABLE: this.os.makeVariable(); break;
			case BUDDY: this.os.makeBuddy(); break;
		}
	}
}
