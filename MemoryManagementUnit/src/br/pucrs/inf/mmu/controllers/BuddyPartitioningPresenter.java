package br.pucrs.inf.mmu.controllers;

import br.pucrs.inf.mmu.models.Memory;
import br.pucrs.inf.mmu.models.NoPartitionFound;
import br.pucrs.inf.mmu.models.Partition;
import br.pucrs.inf.mmu.models.PartitioningMethod;

public class BuddyPartitioningPresenter extends MemoryPresenter {

	@Override
	public String render(Memory memory, PartitioningMethod method)
			throws NoPartitionFound {
		
	
		
		StringBuilder builder = new StringBuilder();
		builder.append("| ");
		
		
		
		
		int free = 0;
		
		for (Partition partition : memory.getPartitions()) {
			
			if (partition.isFree()) {
				
				free += partition.getFreeSize();
				
				int next = memory.getPartitions().indexOf(partition) + 1;
				
				
				// se for o fim das partições ...
				if (next == memory.getPartitions().size()) {
					builder.append(free);
					builder.append(" |");
				} else if (!memory.getPartitions().get(next).isFree()) {
					builder.append(free);
					builder.append(" | ");
					free = 0;
				}
			}
		}
		
		
		
		
		return builder.toString();
		
	}
}
