package br.pucrs.inf.mmu.controllers;


import br.pucrs.inf.mmu.models.Memory;
import br.pucrs.inf.mmu.models.NoPartitionFound;
import br.pucrs.inf.mmu.models.PartitioningMethod;

public abstract class MemoryPresenter {

	
	
	/**
	 * Monta a forma de apresentação da memória de acordo com cada tipo de partição.
	 * 
	 * @param memory a memória para renderização.
	 * @param method o método de martionamento utilizado.
	 * @throws NoPartitionFound caso a memória não tenha sido particionada.
	 * */
	public abstract String render(Memory memory, PartitioningMethod method) throws NoPartitionFound; 
}
