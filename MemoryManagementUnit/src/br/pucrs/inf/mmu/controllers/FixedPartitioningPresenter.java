package br.pucrs.inf.mmu.controllers;

import java.util.List;

import br.pucrs.inf.mmu.models.Memory;
import br.pucrs.inf.mmu.models.NoPartitionFound;
import br.pucrs.inf.mmu.models.PartitioningMethod;
import br.pucrs.inf.mmu.models.Partition;

public class FixedPartitioningPresenter extends MemoryPresenter {

	
	@Override
	public String render(Memory memory, PartitioningMethod method) throws NoPartitionFound {
		
		List<Partition> partitions = memory.getPartitions();
		
		if (partitions == null)
			throw new NoPartitionFound("A memória não foi particionada.");
		
		
		
		StringBuilder builder = new StringBuilder();
		builder.append("| ");
		
		
		int free = 0;
		
		for (Partition partition : partitions) {
			
			free += partition.getFreeSize();
			
			
			int next = partitions.indexOf(partition) + 1;
			
			if (next == partitions.size()) {
				builder.append(free);
				builder.append(" | ");
			} else if (!partitions.get(next).isFree() && free > 0) {
				builder.append(free);
				builder.append(" | ");
				free = 0;
			}
		}
		
		
		return builder.toString();
	}
}
