package br.pucrs.inf.mmu.models;

@SuppressWarnings("serial")
public class AllocationMemoryException extends Exception {

	
	
	public AllocationMemoryException() {
		super();
	}
	
	public AllocationMemoryException(String message) {
		super(message);
	}
}
