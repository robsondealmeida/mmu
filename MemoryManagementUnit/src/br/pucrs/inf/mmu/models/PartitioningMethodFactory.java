package br.pucrs.inf.mmu.models;



/**
 * Fábrica para criação de métodos de particionamento de memória.
 * 
 * @author robsondealmeida
 */
public class PartitioningMethodFactory {

	
	
	private static PartitioningMethodFactory factory;
	
	
	
	private PartitioningMethodFactory() {  }
	
	
	
	/**
	 * Retorna a instância da fábrica de métodos de particionamento.
	 * */
	public static PartitioningMethodFactory getInstance() {
		
		if (factory == null)
			factory = new PartitioningMethodFactory();
		
		
		return factory;
	}



	/**
	 * Cria um tipo de particionamento.
	 * 
	 * @param type é a representação em caracter do tipo de particionamento que deve ser criado.
	 */
	public PartitioningMethod create(char type) throws IllegalArgumentException {
		
		PartitioningMethod partitioningMethod = null;
		
		switch (type) {
			case 'f':
				partitioningMethod = new FixedPartitionMethod();
				partitioningMethod.setPartitioningType(PartitioningMethodType.FIXED);
				break;
			
			case 'v':
				partitioningMethod = new VariablePartitionMethod();
				partitioningMethod.setPartitioningType(PartitioningMethodType.VARIABLE);
				break;
				
			case 'b':
				partitioningMethod = new BuddyPartitionMethod();
				partitioningMethod.setPartitioningType(PartitioningMethodType.BUDDY);
				break;
	
			default:
				throw new IllegalArgumentException(
						"Tipo de particionamento não reconhecido."
				);
		}
		
		
		return partitioningMethod;
	}
}
