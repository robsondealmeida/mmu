package br.pucrs.inf.mmu.models;

@SuppressWarnings("serial")
public class EndOfExecutionException extends Exception {

	
	public EndOfExecutionException() {
		super();
	}
	
	
	public EndOfExecutionException(String message) {
		super(message);
	}
}
