package br.pucrs.inf.mmu.models;

@SuppressWarnings("serial")
public class RequestTypeNotDefined extends Exception {

	
	public RequestTypeNotDefined() {
		super();
	}
	
	
	public RequestTypeNotDefined(String message) {
		super(message);
	}
}
