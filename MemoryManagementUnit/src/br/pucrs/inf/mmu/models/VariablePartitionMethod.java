package br.pucrs.inf.mmu.models;

public class VariablePartitionMethod extends PartitioningMethod {

	
	private AllocationPolicy policy;

	
	
	
	public void setPolicy(char policy) {
		this.policy = AllocationPolicyFactory.getInstance().create(policy);
	}
	
	public AllocationPolicy getAllocationPolicy(){
		return this.policy;
	}

	@Override
	public Memory release(Request request, Memory memory)
			throws ReleaseMemoryException {
		
		
		Partition partition = memory.findPartition(request.getProccess());
		
		if (partition == null)
			throw new ReleaseMemoryException(
					"Processo " + request.getProccess().getId() + " não encontrado na memória."
				);
		

		
		int address = memory.findAddress(partition);
		
		if (address > -1)
			memory.getPartitions().set(address, new Partition(partition.getProccess().getSize()));
		
		
		return memory;
		
	}

	@Override
	public Memory allocate(Request request, Memory memory)
			throws AllocationMemoryException {

		
		return this.policy.allocate(request.getProccess(), memory);
		
	}
}
