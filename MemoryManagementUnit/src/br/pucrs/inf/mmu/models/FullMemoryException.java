package br.pucrs.inf.mmu.models;

@SuppressWarnings("serial")
public class FullMemoryException extends AllocationMemoryException {

	
	public FullMemoryException() {
		super();
	}
	
	
	public FullMemoryException(String message) {
		super(message);
	}
}
