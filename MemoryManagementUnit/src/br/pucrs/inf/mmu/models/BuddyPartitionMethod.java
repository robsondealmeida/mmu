package br.pucrs.inf.mmu.models;

public class BuddyPartitionMethod extends PartitioningMethod {

	@Override
	public Memory release(Request request, Memory memory)
			throws ReleaseMemoryException {
		
		
		Partition target = memory.findPartition(request.getProccess());
		
		if (target == null)
			throw new ReleaseMemoryException(
					"Processo " + request.getProccess().getId() + " não encontrado na memória."
				);
		
		
		target.setProccess(null);
		
		
		
		return memory;
	}
	
	
	@Override
	public Memory allocate(Request request, Memory memory)
			throws AllocationMemoryException {
		
		
		// procura pela primeira partição livre para alocação ...
		Partition start = null;
		
		for (Partition partition : memory.getPartitions()) {
			if (partition.isFree() && partition.getSize() >= request.getProccess().getSize()) {
				start = partition;
				break;
			}
		}
		
		
		// inicia quebra da partição até encontrar um tamanho
		// adequado ao processo ...
		Partition partition = this.part(start, request.getProccess(), memory);		
		partition.setProccess(request.getProccess());
		
		
		
		return memory;
	}


	
	/**
	 * Quebra a partição até encontrar um tamanho de partição mais pŕoximo do tamanho do processo.
	 * 
	 * @param partition partição.
	 * @param proccess processo para alocação.
	 * @param memory memória principal.
	 * */
	private Partition part(Partition partition, Proccess proccess, Memory memory) {
		
		
		boolean fit = (partition.getSize() / 2) < proccess.getSize();
		
		
		// se o processo não couber na metade da partição,
		// ele deve ser alocado na pŕopria partição ...
		if (fit) 
			return partition;
		
		
		
		
		// quebra a partição ao meio ...
		int address = memory.findAddress(partition);
		
		Partition left = new Partition(partition.getSize()/2);
		memory.getPartitions().add(address, left);
		
		
		partition.setSize(partition.getSize()/2);
		

		
		// chama a recursão para a metade esquerda da partição repartida ...
		return this.part(left, proccess, memory);
	}
}
