package br.pucrs.inf.mmu.models;

public class BestFitPolicy extends AllocationPolicy {

	@Override
	public Memory allocate(Proccess proccess, Memory memory)
			throws FullMemoryException {
		
		
		Partition best = new Partition();
		int fit = memory.getSize();
		
		for (Partition partition : memory.getPartitions()) {
			
			int fitting = partition.getSize() - proccess.getSize();

			if (partition.isFree() && fitting > 0 && fitting < fit) {
				best = partition;
				fit = fitting;
			}
		}
		
		return this.fit(best, memory, proccess);
	}
}
