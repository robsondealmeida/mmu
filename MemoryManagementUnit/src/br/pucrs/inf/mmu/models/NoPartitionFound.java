package br.pucrs.inf.mmu.models;

@SuppressWarnings("serial")
public class NoPartitionFound extends Exception {

	
	public NoPartitionFound() {
		super();
	}
	
	public NoPartitionFound(String message) {
		super(message);
	}
}
