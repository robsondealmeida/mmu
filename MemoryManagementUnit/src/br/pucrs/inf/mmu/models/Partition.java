package br.pucrs.inf.mmu.models;

public class Partition {

	private int size;
	private Proccess proccess;

	
	
	public Partition() { }
	
	public Partition(int partitioningSize) {
		this.size = partitioningSize;
	}

	public boolean isFree() {
		return this.proccess == null;
	}

	public int getSize() {
		return this.size;
	}
	
	public void setSize(int size){
		this.size = size;
	}

	public Proccess getProccess() {
		return proccess;
	}

	public void setProccess(Proccess proccess) {
		this.proccess = proccess;
	}
	
	public int getFreeSize() {
		if (this.proccess == null)
			return this.getSize();

		return (this.getSize() - this.getProccess().getSize());
	}
}
