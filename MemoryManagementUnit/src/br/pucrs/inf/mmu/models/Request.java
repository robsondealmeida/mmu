package br.pucrs.inf.mmu.models;

public abstract class Request {

	private Proccess proccess;
	private String representation;
	private RequestType type;

	
	
	
	public void attach(Proccess proccess) {
		this.proccess = proccess;
	}


	public Proccess getProccess() {
		return proccess;
	}
	
	
	public void setRepresentation(String representation) {
		this.representation = representation;
	}
	
	
	public String getRepresentation() {
		return this.representation;
	}


	public RequestType getType() { return this.type; }
	
	
	public void setType(RequestType type) { this.type = type; }
}
