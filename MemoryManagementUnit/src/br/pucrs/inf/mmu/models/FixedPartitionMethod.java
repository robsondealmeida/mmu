package br.pucrs.inf.mmu.models;


public class FixedPartitionMethod extends PartitioningMethod {

	private int partitioningSize;

	
	
	public void setPartitioningSize(int partitioningSize) {
		this.partitioningSize = partitioningSize;
	}
	
	
	public int getPartitioningSize() {
		return this.partitioningSize;
	}
	
	@Override
	public Memory release(Request request, Memory memory) throws ReleaseMemoryException {
		
		
		Partition partition = memory.findPartition(request.getProccess());
		
		if (partition == null)
			throw new ReleaseMemoryException(
					"Processo " + request.getProccess().getId() + " não encontrado na memória."
				);
		
		
		
		int position = memory.findAddress(partition);
		
		if (position > -1)
			memory.getPartitions().set(position, new Partition(this.partitioningSize));
		
		
		return memory;
	}


	@Override
	public Memory allocate(Request request, Memory memory) throws AllocationMemoryException {
		
		if (request.getProccess().getSize() > this.getPartitioningSize())
			throw new AllocationMemoryException(
					"Tamanho do processo supera o tamanho da partição: " + 
					request.getRepresentation() + ", " + this.getPartitioningSize());
		
		
		
		for (Partition partition : memory.getFreePartitions()) {
			if (request.getProccess().getSize() <= partition.getSize()) {
				partition.setProccess(request.getProccess());
				break;
			}
		}
		
 		
		return memory;
	}
}
