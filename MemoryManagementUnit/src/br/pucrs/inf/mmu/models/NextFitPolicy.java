package br.pucrs.inf.mmu.models;

import java.util.ListIterator;

public class NextFitPolicy extends AllocationPolicy {

	
	
	private int last;
	
	
	@Override
	public Memory allocate(Proccess proccess, Memory memory)
			throws FullMemoryException {
		
		
		ListIterator<Partition> iterator = memory.getPartitions().listIterator(last);
		
		while (iterator.hasNext()) {
			
			Partition partition = iterator.next();
			
			if (partition.isFree() && partition.getSize() >= proccess.getSize()) {
				this.last = memory.findAddress(partition);
				return this.fit(partition, memory, proccess);
			}
		}
		
		throw new FullMemoryException(
				"Memória cheia. Antes de alocar mais um processo, faça uma requisição de desalocação."
			);
	}
}
