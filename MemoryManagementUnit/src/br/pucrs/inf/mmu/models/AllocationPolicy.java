package br.pucrs.inf.mmu.models;

public abstract class AllocationPolicy {

	
	/**
	 * Aloca um processo na memória de acordo com a estratégia implementada.
	 * 
	 * @param proccess processo que deve ser alocado.
	 * @param memory memória na qual o processo deve ser alocado.
	 * @throws FullMemoryException quando a memória está cheia e não foi realizada uma 
	 * 			requisção de liberação antes da requisição de alocação.
	 * */
	public abstract Memory allocate(Proccess proccess, Memory memory) throws FullMemoryException;
	
	
	
	/**
	 * Corrige as alocações da memória.
	 * 
	 * @param partition partição na qual o processo deve ser alocado.
	 * @param memory memória que contém as partições e que será corrigida.
	 * @param proccess processo que será alocado.
	 * */
	protected Memory fit(Partition partition, Memory memory, Proccess proccess) {
		
		// cria uma partição para armazenar a sobra da alocação ...
		Partition remainder = new Partition((partition.getSize() - proccess.getSize()));
		
		// se for a última partição da memória, utiliza o endereço ...
		int address = (memory.findAddress(partition) == (memory.getPartitions().size() - 1))
						? memory.findAddress(partition)
						: memory.findAddress(partition) + 1;
		
		if (address == 0 || (address + 1) == memory.getPartitions().size())
			memory.getPartitions().add(remainder);
		else
			memory.getPartitions().add(address, remainder);
		
		
		partition.setProccess(proccess);
		partition.setSize(proccess.getSize());
		
		return memory;
	}
}
