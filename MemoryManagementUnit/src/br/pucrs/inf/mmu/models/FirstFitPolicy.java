package br.pucrs.inf.mmu.models;



/**
 * Política de alocação First-Fit.
 * */
public class FirstFitPolicy extends AllocationPolicy {

	@Override
	public Memory allocate(Proccess proccess, Memory memory) throws FullMemoryException {
		
		
		for (Partition partition : memory.getPartitions()) {
			
			if (partition.isFree() && partition.getSize() >= proccess.getSize()) {
				
				return this.fit(partition, memory, proccess);
			}
		}
		
		throw new FullMemoryException(
				"Memória cheia. Antes de alocar mais um processo, faça uma requisição de desalocação."
			); 
	}
}




