package br.pucrs.inf.mmu.models;

@SuppressWarnings("serial")
public class ReleaseMemoryException extends Exception {

	
	public ReleaseMemoryException() {
		super();
	}
	
	public ReleaseMemoryException(String message) {
		super(message);
	}
}
