package br.pucrs.inf.mmu.models;



/**
 * Representa um método de particionamento de memória.
 * 
 * @author robsondealmeida
 */
public abstract class PartitioningMethod {

	
	private PartitioningMethodType type;
	
	
	
	/*
	 * Retorna o tipo do método particionamento.
	 * */
	public PartitioningMethodType getPartitioningMethodType() {
		return this.type;
	}


	/*
	 * Define o tipo de método de particionamento utilizado.
	 * 
	 * @param type é tipo de método. 
	 */
	public void setPartitioningType(PartitioningMethodType type) {
		this.type = type;
	}


	
	/**
	 * Aloca espaço na memória para entrada de um novo processo.
	 * */
	public abstract Memory allocate(Request request, Memory memory) throws AllocationMemoryException;
	
	
	
	/**
	 * Libera espaço na memória.
	 * */
	public abstract Memory release(Request request, Memory memory) throws ReleaseMemoryException;


}
