package br.pucrs.inf.mmu.models;

import java.util.LinkedList;
import java.util.Queue;



/**
 * Representa um sistema operacional.
 * 
 * @author robsondealmeida
 */
public class OperationalSystem {

	
	private PartitioningMethod method;
	private Queue<Request> requests;
	private Memory memory;

	
	
	public OperationalSystem() {
		this.requests = new LinkedList<Request>();
		this.memory = new Memory();
	}
	
	
	
	/**
	 * Define o método de particionamento da memória.
	 */
	public void setPartitioningMethod(PartitioningMethod partitioningMethod) {
		this.method = partitioningMethod;
	}

	/**
	 * Retorna o método de particionamento da memória.
	 */
	public PartitioningMethod getPartitioningMethod() {
		return method;
	}


	/**
	 * Coloca uma requisição na fila do sistema.
	 */
	public void addRequest(Request request) {
		this.requests.add(request);
	}
	
	
	/**
	 * Retorna pilha fila de requisições do sistema.
	 */
	public Queue<Request> getRequests() {
		return this.requests;
	}



	/**
	 * Define o tamanho da memória.
	 * */
	public void setMemorySize(int memorySize) {
		this.memory.setSize(memorySize);
	}


	/**
	 * Retorna a memória.
	 * */
	public Memory getMemory() {
		return this.memory;
	}



	public void proccessRequest(Request request) 
			throws AllocationMemoryException, ReleaseMemoryException, RequestTypeNotDefined {
		
		
		switch (request.getType()) {
		
			case IN: 
				this.memory = this.method.allocate(request, this.memory); 
				break;
			
			case OUT:
				this.memory = this.method.release(request, this.memory); 
				break;
				
			default:
				throw new RequestTypeNotDefined(
						"Tipo de requisição não reconhecida. " +
						"Certifique-se que o arquivo de requisições está correto."
					);
		}
		
	}



	public void makeFixed() {
		
		
		FixedPartitionMethod fixedPartitionMethod = (FixedPartitionMethod)this.getPartitioningMethod();		
		
		
		int breaks = this.memory.getSize() / fixedPartitionMethod.getPartitioningSize();
		
		while(breaks > 0) {
			this.memory.getPartitions()
					.add(new Partition(fixedPartitionMethod.getPartitioningSize()));
			
			breaks--;
		}
	}



	public void makeVariable() {
		
		this.memory.getPartitions().add(new Partition(this.memory.getSize()));
	}



	public void makeBuddy() {
		
		this.memory.getPartitions().add(new Partition(this.memory.getSize()));
	}
}
