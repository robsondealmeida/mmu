package br.pucrs.inf.mmu.models;

import java.util.ArrayList;
import java.util.List;

public class Memory {
	
	private int size;
	private List<Partition> partitions;
	
	
	
	public Memory() {
		this.partitions = new ArrayList<Partition>();
	}
	

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	
	/**
	 * Seleciona as partições livres da memória.
	 * */
	public List<Partition> getFreePartitions() {
		
		List<Partition> free = new ArrayList<Partition>();
		
		for (Partition partition : this.partitions)
			if (partition.isFree())
				free.add(partition);
		
		
		return free;
	}


	public List<Partition> getPartitions() {
		return this.partitions;
	}


	public Partition findPartition(Proccess proccess) {
		
		for (Partition partition : this.partitions) {
			if (!partition.isFree() && partition.getProccess().getId().equals(proccess.getId()))
				return partition;
		}
			
		
		
		return null;
	}


	
	/**
	 * Encontra o endereço (índice) de uma partição na memória.
	 * 
	 * @param partition partição buscada.
	 * @return o endereço da partição ou -1 caso a partição não esteja na memória.
	 * */
	public int findAddress(Partition partition) {
		
		return this.getPartitions().indexOf(partition);
	}
}
