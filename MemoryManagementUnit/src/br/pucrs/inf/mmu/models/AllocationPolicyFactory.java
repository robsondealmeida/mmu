package br.pucrs.inf.mmu.models;



/*
 * Fábrica para criação de políticas de alocação.
 * */
public class AllocationPolicyFactory {

	
	private static AllocationPolicyFactory policyFactory;
	
	
	
	/*
	 * Retorna a instância única da fábrica.
	 * */
	public static AllocationPolicyFactory getInstance() {
		if (policyFactory == null)
			policyFactory = new AllocationPolicyFactory();
		
		return policyFactory;
	}

	
	/*
	 * Cria uma política de alocação de acordo com o argumento.
	 * @param policy tipo de poítica (b, w, f ou n).
	 * @return uma política de alocação.
	 * */
	public AllocationPolicy create(char policy) {
		
		switch (policy) {
			case 'b' : return new BestFitPolicy();
			case 'w' : return new WorstFitPolicy();
			case 'f' : return new FirstFitPolicy();
			case 'n' : return new NextFitPolicy();
			
			default: 
				throw new IllegalArgumentException("Política de alocação não reconhecida.");
		}
	}
}
