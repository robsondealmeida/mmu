package br.pucrs.inf.mmu.models;

public enum PartitioningMethodType {
	
	FIXED, VARIABLE, BUDDY
}
