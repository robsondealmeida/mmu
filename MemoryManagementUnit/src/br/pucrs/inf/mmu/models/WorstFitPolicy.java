package br.pucrs.inf.mmu.models;

public class WorstFitPolicy extends AllocationPolicy {

	@Override
	public Memory allocate(Proccess proccess, Memory memory)
			throws FullMemoryException {
		
		Partition worst = new Partition();
		int most = 0;
		
		for (Partition partition : memory.getPartitions()) {
			
			int diff = partition.getSize() - proccess.getSize();
			
			if (partition.isFree() && diff > 0 && diff > most) {
				worst = partition;
				most = diff;
			}
		}
		
		
		return this.fit(worst, memory, proccess);
	}
}
